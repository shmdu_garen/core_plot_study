//
//  main.m
//  core_plot_study
//
//  Created by dushumeng on 14-1-17.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DSMAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DSMAppDelegate class]));
    }
}
