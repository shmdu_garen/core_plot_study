//
//  DSMStockKlineTestViewController.h
//  core_plot_study
//
//  Created by dushumeng on 14-1-27.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSMStockBaseViewController.h"
#import "DSMStockKlineTestView.h"
#import "DSMStockKlineVolumeTestView.h"
#import "DSMStockKlineTestView.h"
#import "DSMstockKlineTestData.h"

@interface DSMStockKlineTestViewController : DSMStockBaseViewController {
    NSDictionary *stopStartObj;
    NSDictionary *stopEndObj;
    int nScale;
}

@end
