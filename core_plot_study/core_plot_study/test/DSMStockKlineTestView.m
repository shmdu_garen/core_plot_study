//
//  DSMStockKlineTestView.m
//  core_plot_study
//
//  Created by dushumeng on 14-1-27.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#import "DSMStockKlineTestView.h"
#import "DSMStockPriceMovementUtil.h"

@implementation DSMStockKlineTestView

- (void)changeXYAxisWithXLen:(int)xLen dataSource:(NSArray *)dataSource
{
    float maxValueY = 0.0f, minValueY = 0.0f;
    
    // 先循环一次datasource获得最高价和最低价
    for (NSDictionary *map in dataSource) {
        float mZGCJ = [(NSNumber *)[map objectForKey:@"HIGH"] floatValue];
        float mZDCJ = [(NSNumber *)[map objectForKey:@"LOW"] floatValue];
        float mADJ = [(NSNumber *)[map objectForKey:@"ADJ"] floatValue];
        float max = [self getMaxWithNum1:mZDCJ num2:mZGCJ num3:mADJ];
        float min = [self getMinWithNum1:mZDCJ num2:mZGCJ num3:mADJ];
        
        if (maxValueY == 0 || max > maxValueY) {
            maxValueY = max;
        }
        if (minValueY == 0 || min < minValueY) {
            minValueY = min;
        }
    }
    
    float yStart = minValueY - 0.1 < 0 ? 0 : minValueY - 0.1;
    float yEnd = maxValueY + 0.1;
    
    CPTXYGraph *graph = (CPTXYGraph *)super.hostingView.hostedGraph;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    // 设置y轴的长度及起点
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromCGFloat(yStart) length:CPTDecimalFromCGFloat(yEnd - yStart)];
    // 设置x轴的长度及起点
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromCGFloat(-1) length:CPTDecimalFromInt(xLen + 1)];
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    // 设置y轴
    CPTXYAxis *y = axisSet.yAxis;
    // Y轴原点
    y.orthogonalCoordinateDecimal = CPTDecimalFromFloat(-1);
    y.majorIntervalLength = CPTDecimalFromInt(0.15);
    y.labelingPolicy = CPTAxisLabelingPolicyAutomatic;
    // Y轴精度
    NSNumberFormatter *yLabelFormatter = [[NSNumberFormatter alloc] init];
    [yLabelFormatter setMaximumFractionDigits:3];
    [yLabelFormatter setMinimumFractionDigits:2];
    y.labelFormatter = yLabelFormatter;
    [DSMStockPriceMovementUtil changeYAxisMajorTick:y start:yStart end:yEnd];
    
    // 设置x轴
    CPTXYAxis *x = axisSet.xAxis;
    // 设置x轴原点
    x.orthogonalCoordinateDecimal = CPTDecimalFromCGFloat(yStart);
    x.majorIntervalLength = CPTDecimalFromInt(1);
    [self changeXLabel:x dataSource:dataSource];
}

- (void)changeXLabel:(CPTXYAxis *)xAxis dataSource:(NSArray *)dataSource
{
    NSMutableDictionary *labels = [NSMutableDictionary dictionaryWithCapacity:5];
    //    NSArray *indexs = [NSArray arrayWithObjects:[NSNumber numberWithInt:0], [NSNumber numberWithInt:[dataSource count] - 1], nil];
    //    for (NSNumber *idx in indexs) {
    //        NSDictionary *infoMap = (NSDictionary *)[dataSource objectAtIndex:[idx intValue]];
    //        [labels setObject:[NSNumber numberWithInt:([idx intValue] + 1)] forKey:[infoMap objectForKey:@"DATA"]];
    //    }
    [DSMStockPriceMovementUtil drawAxisLabel:xAxis labels:labels textColor:nil];
}

- (void)createMyPlot
{
    
    NSMutableArray *array = super.plotArray;
    CPTXYGraph *graph = (CPTXYGraph *)super.hostingView.hostedGraph;
    
    // 创建粗柱
    CPTBarPlot *kpPlot = [[CPTBarPlot alloc] init];
    // 设置数据源
    kpPlot.dataSource = delegate;
    kpPlot.delegate = delegate;
    // 设置柱状图的线条
    CPTMutableLineStyle *borderLineStyle = [CPTMutableLineStyle lineStyle];
	borderLineStyle.lineWidth = 0.0f;
    kpPlot.lineStyle = borderLineStyle;
    // 设置柱状图的宽度
    kpPlot.barWidth = CPTDecimalFromString(@"0.8");
    // 设置柱状图的开始位置
    kpPlot.baseValue = CPTDecimalFromString(@"0");
    // 设置柱状图的底部是否可变
    kpPlot.barBasesVary = YES;
    // 设置tag
    kpPlot.identifier = kPlotKP;
    // 设置偏移
    //kpPlot.barOffset = CPTDecimalFromString(@"0.5");
    // 添加到图层
    [graph addPlot:kpPlot];
    // 添加到list中
    [array addObject:kpPlot];
    
    // 创建粗柱
    CPTBarPlot *cjPlot = [[CPTBarPlot alloc] init];
    // 设置数据源
    cjPlot.dataSource = delegate;
    cjPlot.delegate = delegate;
    // 设置柱状图的线条
    CPTMutableLineStyle *cjPlotLineStyle = [CPTMutableLineStyle lineStyle];
	cjPlotLineStyle.lineWidth = 0.0f;
    cjPlot.lineStyle = borderLineStyle;
    // 设置柱状图的宽度
    cjPlot.barWidth = CPTDecimalFromString(@"0.1");
    // 设置柱状图的开始位置
    cjPlot.baseValue = CPTDecimalFromString(@"0");
    // 设置柱状图的底部是否可变
    cjPlot.barBasesVary = YES;
    // 设置tag
    cjPlot.identifier = kPlotCJ;
    // 设置偏移
    //cjPlot.barOffset = CPTDecimalFromString(@"0.5");
    // 添加到图层
    [graph addPlot:cjPlot];
    // 添加到list中
    [array addObject:cjPlot];
    
    CPTScatterPlot *avgPricePlot = [[CPTScatterPlot alloc] init];
    avgPricePlot.identifier = kPlotAvg;
    avgPricePlot.dataSource = delegate;
    avgPricePlot.delegate = delegate;
    CPTMutableLineStyle *lineStyle = [CPTMutableLineStyle lineStyle];
    lineStyle.miterLimit = 1.0f;
    lineStyle.lineWidth = 1.0f;
    lineStyle.lineColor = [CPTColor redColor];
    avgPricePlot.dataLineStyle = lineStyle;
    avgPricePlot.opacity = 1.0f;
    
    [graph addPlot:avgPricePlot];
    [array addObject:avgPricePlot];
}

- (float) getMaxWithNum1:(float)num1 num2:(float)num2 num3:(float)num3
{
    float max = num1;
    max = max > num2 ? max : num2;
    max = max > num3 ? max : num3;
    return max;
}

- (float) getMinWithNum1:(float)num1 num2:(float)num2 num3:(float)num3
{
    float min = num1;
    min = min < num2 ? min : num2;
    min = min < num3 ? min : num3;
    return min;
}


@end
