//
//  DSMStockKlineTestData.h
//  core_plot_study
//
//  Created by shmdu on 14-4-4.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "FMDB.h"

@interface DSMStockKlineTestData : NSObject {
    sqlite3 *database;
}

- (NSArray *)loadStockTestData;

@end
