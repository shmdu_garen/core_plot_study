//
//  DSMStockKlineTestData.m
//  core_plot_study
//
//  Created by shmdu on 14-4-4.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#import "DSMStockKlineTestData.h"

@implementation DSMStockKlineTestData

- (NSArray *) loadStockTestData
{
    NSMutableArray *data = [[NSMutableArray alloc] init];
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *defaultDBPath = [[documentsDirectory stringByAppendingPathComponent:@"db"] stringByAppendingPathComponent:@"test.db"];
    
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"test.db"];
    NSLog(@"db path is %@", defaultDBPath);
    FMDatabase *db = [FMDatabase databaseWithPath:defaultDBPath];
    [db open];
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM test_stock_kline ORDER BY SID DESC"];
    while ([rs next]) {
        NSString *date_value = [rs stringForColumn:@"DATA"];
        double open_value = [rs doubleForColumn:@"OPEN"];
        double high_value = [rs doubleForColumn:@"HIGH"];
        double low_value = [rs doubleForColumn:@"LOW"];
        double close_value = [rs doubleForColumn:@"CLOSE"];
        long volume_value = [rs longForColumn:@"VOLUME"];
        double adj_value = [rs doubleForColumn:@"ADJ"];
        
        NSMutableDictionary *infoMap = [NSMutableDictionary dictionaryWithCapacity:6];
        [infoMap setValue:date_value forKey:@"DATA"];
        [infoMap setValue:[NSNumber numberWithFloat:open_value] forKey:@"OPEN"];
        [infoMap setValue:[NSNumber numberWithFloat:high_value] forKey:@"HIGH"];
        [infoMap setValue:[NSNumber numberWithFloat:low_value] forKey:@"LOW"];
        [infoMap setValue:[NSNumber numberWithFloat:close_value] forKey:@"CLOSE"];
        [infoMap setValue:[NSNumber numberWithLong:volume_value] forKey:@"VOLUME"];
        [infoMap setValue:[NSNumber numberWithFloat:adj_value] forKey:@"ADJ"];
        
        NSLog(@"@date:%@;open:%f;high:%f;low:%f;close:%f;volume:%li;adj:%f", date_value, open_value, high_value, low_value, close_value, volume_value, adj_value);
        
        [data addObject:infoMap];
    }
    [rs close];
    [db close];
    return [data mutableCopy];
}


+ (void) copyDB
{
    
    //数据库沙箱路径
    NSString *databasePath = @"db";
    
    //数据库文件名
    NSString *databaseName = @"test.db";
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //创建目录
    NSString *databaseDirectory = [documentsDirectory stringByAppendingPathComponent:databasePath];
    BOOL isDirectorySuccess= [fileManager createDirectoryAtPath:databaseDirectory withIntermediateDirectories:YES attributes:nil error:&error];
    if(!isDirectorySuccess)
    {
        NSLog(@"向沙箱创建目录出错！错误信息：%@", [error localizedDescription]);
    }
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",databasePath,databaseName ]];
    BOOL success = [fileManager fileExistsAtPath:writableDBPath];
    if (success){
        return;
    }
    //文件删除
    if (success&&[fileManager removeItemAtPath:writableDBPath error:&error] != YES){
        NSLog(@"删除文件出错！错误信息：%@", [error localizedDescription]);
        return;
    }
    
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSLog(@"向沙箱拷贝数据库文件出错！错误信息：%@", [error localizedDescription]);
    }
}


@end
