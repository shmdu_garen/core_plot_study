//
//  DSMStockKlineVolumeTestView.h
//  core_plot_study
//
//  Created by shmdu on 14-4-4.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#define kPlotVolume @"plot_volume"

#import "DSMStockBaseView.h"

@interface DSMStockKlineVolumeTestView : DSMStockBaseView <DSMStockBaseViewProtocol>

@end
