//
//  DSMStockKlineTestView.h
//  core_plot_study
//
//  Created by dushumeng on 14-1-27.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#define kPlotKP @"plot_kp"
#define kPlotCJ @"plot_cj"
#define kPlotAvg  @"plot_AVG"


#import "DSMStockBaseView.h"

@interface DSMStockKlineTestView : DSMStockBaseView

@end
