//
//  DSMStockKlineTestViewController.m
//  core_plot_study
//
//  Created by dushumeng on 14-1-27.
//  Copyright (c) 2014年 garen. All rights reserved.
//

// view默认宽度
#define DEFAULT_VIEW_WIDTH 750
#define DEFAULT_SCALE_LEN 25
#define DEFAULT_MAX_SCALE 4

#import "DSMStockKlineTestViewController.h"
#import "DSMStockHeader.h"
#import "UIColor+DSMStyle.h"

@interface DSMStockKlineTestViewController ()

@end

@implementation DSMStockKlineTestViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    return [self init];
}

- (id)init
{
    self = [super init];
    if (self) {
        xAxisLen = 75;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#0f1519"];
    
    DSMStockPadding uiviewPadding = {80,15,0,0};
    DSMStockDrawInfo uiview1DrawInfo = {YES,YES,YES,NO,YES};
    DSMStockGesture uiview1Gesture = {YES,YES,YES};
    
    DSMStockKlineTestView *uiview1 = [[DSMStockKlineTestView alloc] initWithFrame:CGRectMake(0, 100, DEFAULT_VIEW_WIDTH, 350) tag:@"kline" padding:uiviewPadding drawInfo:uiview1DrawInfo gesture:uiview1Gesture delegate:self];
    // uiview1.backgroundColor = [UIColor randomColor];
    
    [stockBaseViewMap setObject:uiview1 forKey:@"kline"];
    DSMStockDrawInfo uiview2DrawInfo = {NO,NO,NO,YES,NO};
    DSMStockGesture uiview2Gesture = {NO, NO, NO};
    
    DSMStockKlineVolumeTestView *uiview2 = [[DSMStockKlineVolumeTestView alloc] initWithFrame:CGRectMake(0, 460, DEFAULT_VIEW_WIDTH, 150) tag:@"kline" padding:uiviewPadding drawInfo:uiview2DrawInfo gesture:uiview2Gesture delegate:self];
    // uiview2.backgroundColor = [UIColor randomColor];
    [stockBaseViewMap setObject:uiview2 forKey:@"volume"];
    NSArray *array = [[DSMStockKlineTestData new] loadStockTestData];
    dataSource = [array mutableCopy];
    
    showRange.location = 0;
    [self dataSourceChange];
    
    [self.view addSubview:uiview1];
    [self.view addSubview:uiview2];
}

- (NSString *)showValueWithType:(NSString *)type :(NSObject *)obj
{
    NSDictionary *infoMap = (NSDictionary *)obj;
    if ([type isEqualToString:TAG_TOP]) {
        return [NSString stringWithFormat:@"  日期：%i 开：%.2f 高：%.2f 低：%.2f 收：%.2f 成交量：%lli",
                [[infoMap objectForKey:@"DATA"] intValue]
                , [(NSNumber *)[infoMap objectForKey:@"OPEN"] floatValue]
                , [(NSNumber *)[infoMap objectForKey:@"HIGH"] floatValue]
                , [(NSNumber *)[infoMap objectForKey:@"LOW"] floatValue]
                , [(NSNumber *)[infoMap objectForKey:@"CLOSE"] floatValue]
                , [[infoMap objectForKey:@"VOLUME"] longLongValue]];
    } else if ([type isEqualToString:TAG_LEFT]) {
        return @"";
    } else if ([type isEqualToString:TAG_BOTTOM]) {
        return [infoMap objectForKey:@"DATA"];
    } else if ([type isEqualToString:TAG_RIGHT]) {
        return @"";
    }
    return @"";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)loadDataByPan:(CGFloat)len tag:(NSString *)tag
{
    loadingData = YES;
    if (len > 0) {
        // 右移, 向后取数据
        if (showRange.location == 0) {
            loadingData = NO;
        } else {
            showRange.location = showRange.location < 20 ? 0 : showRange.location - 20;
            [self dataSourceChange];
            loadingData = NO;
        }
    } else {
        int mLocation = showRange.location + 20;
        showRange.location = mLocation >= [dataSource count] ? [dataSource count] - 20 : mLocation;
        [self dataSourceChange];
        loadingData = NO;
    }
}

- (void)loadDataByPinch:(CGFloat)scale tag:(NSString *)tag
{
}

- (CPTFill *)barFillForBarPlot:(CPTBarPlot *)barPlot recordIndex:(NSUInteger)idx
{
    NSDictionary *infomap = [showDataSource objectAtIndex:idx];
    NSNumber *m1 = [NSNumber numberWithFloat:[[infomap objectForKey:@"OPEN"] floatValue]];
    NSNumber *m2 = [NSNumber numberWithFloat:[[infomap objectForKey:@"CLOSE"] floatValue]];
    if ([m1 isLessThan:m2]) {
        return [CPTFill fillWithColor:[CPTColor colorWithHexString:COLOR_LINE_RED]];
    } else {
        return [CPTFill fillWithColor:[CPTColor colorWithHexString:COLOR_LINE_BLUE]];
    }
}

- (NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)idx
{
    NSDictionary *infomap = [showDataSource objectAtIndex:idx];
    
    NSNumber *num = nil;
    
    NSString *key = (NSString *)plot.identifier;
    
    if ([key isEqualToString:kPlotKP]) {
        if (fieldEnum == CPTBarPlotFieldBarLocation) {
            num = [NSNumber numberWithInt:idx];
        } else if (fieldEnum == CPTBarPlotFieldBarTip || fieldEnum == CPTBarPlotFieldBarBase) {
            NSNumber *m1 = [NSNumber numberWithFloat:[[infomap objectForKey:@"OPEN"] floatValue]];
            NSNumber *m2 = [NSNumber numberWithFloat:[[infomap objectForKey:@"CLOSE"] floatValue]];
            if (fieldEnum == CPTBarPlotFieldBarTip) {
                num = [m1 isLessThan:m2] ? m2 : m1;
            } else if (fieldEnum == CPTBarPlotFieldBarBase) {
                num = [m1 isLessThan:m2] ? m1 : m2;
            }
        }
    } else if ([key isEqualToString:kPlotCJ]) {
        if (fieldEnum == CPTBarPlotFieldBarLocation) {
            num = [NSNumber numberWithInt:idx];
        } else if (fieldEnum == CPTBarPlotFieldBarTip) {
            num = [NSNumber numberWithFloat:[[infomap objectForKey:@"HIGH"] floatValue]];
        } else if (fieldEnum == CPTBarPlotFieldBarBase) {
            num = [NSNumber numberWithFloat:[[infomap objectForKey:@"LOW"] floatValue]];
        }
    } else if ([key isEqualToString:kPlotAvg]) {
        if(fieldEnum == CPTScatterPlotFieldX){
            num = [NSNumber numberWithInt:idx];
        } else if (fieldEnum == CPTScatterPlotFieldY) {
            num = [NSNumber numberWithFloat:[[infomap objectForKey:@"ADJ"] floatValue]];
        }
        
    } else if ([key isEqualToString:kPlotVolume]) {
        if(fieldEnum == CPTScatterPlotFieldX){
            num = [NSNumber numberWithInt:idx];
        } else if (fieldEnum == CPTScatterPlotFieldY) {
            num = [NSNumber numberWithFloat:[[infomap objectForKey:@"VOLUME"] floatValue]];
        }
    }
    return num;
}

@end
