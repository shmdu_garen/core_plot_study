//
//  DSMAppDelegate.h
//  core_plot_study
//
//  Created by dushumeng on 14-1-17.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSMStockKlineTestData.h"
#import "DSMStockKlineTestViewController.h"

@interface DSMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
