//
//  DSMStockBaseView.h
//  core_plot_study
//
//  Created by dushumeng on 14-1-27.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#define COLOR_LINE_RED @"#e63e51"
#define COLOR_LINE_BLUE @"#1f80bf"

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "DSMStockHeader.h"
#import "DSMStockGraphHostingView.h"
#import "DSMStockCrossShapedView.h"

/**
 * @brief DSMStockBaseViewProtocol
 * 股票行情绘图协议，子类实现
 */
@protocol DSMStockBaseViewProtocol <NSObject>
/**
 * @brief       修改xy轴的设置
 * @param       xLen        x轴长度
 * @param       dataSource  数据源
 * @return      NSArray
 */
- (void)changeXYAxisWithXLen:(int)xLen dataSource:(NSArray *)dataSource;
/**
 * @brief       创建需要绘制的图形，要将创建的图形加入的数组中
 */
- (void)createMyPlot;
@end

/**
 * @brief DSMStockBaseViewDelegate
 * 将事件交给委托处理
 */
@protocol DSMStockBaseViewDelegate <CPTScatterPlotDataSource, CPTScatterPlotDelegate, CPTBarPlotDataSource, CPTBarPlotDelegate>
@required
- (void)whenTapEvent:(UITapGestureRecognizer *)sender tag:(NSString *)tag;
- (void)whenPanEvent:(UIPanGestureRecognizer *)sender tag:(NSString *)tag;
- (void)whenPinchEvent:(UIPinchGestureRecognizer *)sender tag:(NSString *)tag;
- (NSString *)showValueWithType:(NSString *)type :(NSObject *)obj;
/**
 * @brief       显示的数据源
 * @return      NSArray
 */
- (NSArray *)showDataSource;
/**
 * @brief       获取x轴长度
 */
- (int)xAxisLen;
@end


@interface DSMStockBaseView : UIView <DSMStockCrossShapedViewDelegate, DSMStockBaseViewProtocol> {
    __weak id<DSMStockBaseViewDelegate> delegate; //!< 委托.
    DSMStockPadding padding; //!< CPTGraph的padding.
    NSString *tag; //!< 标签.
}
/**
 * @brief 绘制十字线的view
 */
@property (nonatomic, readwrite, strong) DSMStockCrossShapedView *crossView;
/**
 * @brief 绘制十字线的view
 */
@property (nonatomic, readwrite, strong) DSMStockGraphHostingView *hostingView;
/**
 * @brief plotArray
 */
@property (nonatomic, readwrite, strong) NSMutableArray *plotArray;
/**
 * @brief       初始化方法
 * @param       frame
 * @param       mTag
 * @param       mPadding
 * @param       mDrawInfo
 * @param       mGesture
 * @param       mDelegate
 */
- (id)initWithFrame:(CGRect)frame tag:(NSString *)mTag padding:(DSMStockPadding)mPadding drawInfo:(DSMStockDrawInfo)mDrawInfo gesture:(DSMStockGesture)mGesture delegate:(id)mDelegate;
/**
 * @brief       数据源修改
 */
- (void)dataSourceChanged;
/**
 * @brief       画十字线
 * @param       point
 * @param       infoMap
 */
- (void)drawCrossLine:(CGPoint)point info:(NSDictionary *)infoMap;
/**
 * @brief       隐藏十字线
 */
- (void)hideCrossLine;

@end
