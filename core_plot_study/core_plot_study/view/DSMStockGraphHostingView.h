//
//  DSMStockGraphHostingView.h
//  core_plot_study
//
//  Created by dushumeng on 14-1-20.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "DSMStockHeader.h"

@interface DSMStockGraphHostingView : CPTGraphHostingView
/**
 * @brief       获得CPTGraph的padding
 * @return      KDVIEWStockPadding
 */
- (DSMStockPadding) stockPadding;
@end
