//
//  DSMStockGraphHostingView.m
//  core_plot_study
//
//  Created by dushumeng on 14-1-20.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#import "DSMStockGraphHostingView.h"

@implementation DSMStockGraphHostingView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (DSMStockPadding) stockPadding
{
    CPTGraph *hostedGraph = self.hostedGraph;
    DSMStockPadding padding = {
        hostedGraph.paddingLeft + [hostedGraph.plotAreaFrame paddingLeft],
        hostedGraph.paddingTop + [hostedGraph.plotAreaFrame paddingTop],
        hostedGraph.paddingRight + [hostedGraph.plotAreaFrame paddingRight],
        hostedGraph.paddingBottom + [hostedGraph.plotAreaFrame paddingBottom]
    };
    return padding;
}

@end
