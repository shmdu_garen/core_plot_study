//
//  DSMViewController.m
//  core_plot_study
//
//  Created by dushumeng on 14-1-17.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#import "DSMViewController.h"
#import "DSMStockKlineTestData.h"

@interface DSMViewController ()

@end

@implementation DSMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[[DSMStockKlineTestData alloc] init] loadStockTestData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
