//
//  DSMStockTheme.m
//  core_plot_study
//
//  Created by dushumeng on 14-1-20.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#import "DSMStockTheme.h"
#import "UIColor+DSMStyle.h"

#define COLOR_BLACK @"#070a0c"
#define COLOR_GRAY @"#1e1f20"
#define COLOR_XY_LINE @"#2e2e2e"
#define COLOR_MAJOR_GRID_LINE @"#282929"

@implementation DSMStockTheme

-(id)init
{
    if ((self = [super init])) {
        self.graphClass = [CPTXYGraph class];
    }
    return self;
}

-(id)newGraph
{
    CPTXYGraph *graph;
    
    if (self.graphClass) {
        graph = [(CPTXYGraph *)[self.graphClass alloc] initWithFrame:CPTRectMake(0.0, 0.0, 200.0, 200.0)];
    }
    else {
        graph = [(CPTXYGraph *)[CPTXYGraph alloc] initWithFrame:CPTRectMake(0.0, 0.0, 200.0, 200.0)];
    }
    graph.paddingLeft   = CPTFloat(60.0);
    graph.paddingTop    = CPTFloat(60.0);
    graph.paddingRight  = CPTFloat(60.0);
    graph.paddingBottom = CPTFloat(60.0);
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromDouble(-1.0) length:CPTDecimalFromDouble(1.0)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromDouble(-1.0) length:CPTDecimalFromDouble(1.0)];
    
    [self applyThemeToGraph:graph];
    
    return graph;
}

#pragma mark -
#pragma mark NSCoding Methods

-(Class)classForCoder
{
    return [CPTTheme class];
}

- (void)applyThemeToBackground:(CPTGraph *)graph
{
    // 设置graph背景
    graph.fill = [CPTFill fillWithColor:[CPTColor clearColor]];
}

- (void)applyThemeToPlotArea:(CPTPlotAreaFrame *)plotAreaFrame
{
    // 设置plotArea风格
    CPTGradient *stocksBackgroundGradient = [[CPTGradient alloc] init];
    
    UIColor *blackColor = [UIColor colorWithHexString:COLOR_BLACK];
    UIColor *grayColcor = [UIColor colorWithHexString:COLOR_GRAY];
    
    // 交叉设置背景颜色
    int j = 0;
    for (float i = 0 ; i <= 1.0 ; i+=1.0/8) {
        j++;
        if (i == 0.0) {
            stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPTColor colorWithCGColor:blackColor.CGColor] atPosition:0.0];
        } else if (i == 1.0){
            stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPTColor colorWithCGColor:grayColcor.CGColor] atPosition:1];
        } else {
            if (j % 2 == 0) {
                stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPTColor colorWithCGColor:blackColor.CGColor] atPosition:i];
                stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPTColor colorWithCGColor:grayColcor.CGColor] atPosition:i];
            } else {
                stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPTColor colorWithCGColor:grayColcor.CGColor] atPosition:i];
                stocksBackgroundGradient = [stocksBackgroundGradient addColorStop:[CPTColor colorWithCGColor:blackColor.CGColor] atPosition:i];
            }
        }
    }
    // 设置旋转0，90，180，270
    stocksBackgroundGradient.angle = CPTFloat(0);
    plotAreaFrame.fill             = [CPTFill fillWithGradient:stocksBackgroundGradient];
    
    CPTMutableLineStyle *borderLineStyle = [CPTMutableLineStyle lineStyle];
    borderLineStyle.lineColor = [CPTColor colorWithGenericGray:CPTFloat(0.2)];
    borderLineStyle.lineWidth = CPTFloat(0.0);
    
    plotAreaFrame.borderLineStyle = borderLineStyle;
    plotAreaFrame.cornerRadius    = CPTFloat(0);
}

- (void)applyThemeToAxisSet:(CPTAxisSet *)axisSet
{
    // 设置坐标系风格
    CPTXYAxisSet *xyAxisSet             = (CPTXYAxisSet *)axisSet;
    
    // X、Y轴样式
    CPTMutableLineStyle *xyLineStyle = [CPTMutableLineStyle lineStyle];
    UIColor *xyLineColor = [UIColor colorWithHexString:COLOR_XY_LINE];
    xyLineStyle.lineColor = [CPTColor colorWithCGColor:xyLineColor.CGColor];
    xyLineStyle.lineWidth = CPTFloat(5.0);
    
    // 大刻度样式
    CPTMutableLineStyle *majorLineStyle = [CPTMutableLineStyle lineStyle];
    majorLineStyle.lineCap   = kCGLineCapRound;
    majorLineStyle.lineColor = [CPTColor clearColor];
    majorLineStyle.lineWidth = CPTFloat(3.0);
    
    // 小刻度样式
    CPTMutableLineStyle *minorLineStyle = [CPTMutableLineStyle lineStyle];
    minorLineStyle.lineColor = [CPTColor clearColor];
    minorLineStyle.lineWidth = CPTFloat(3.0);
    
    // 设置X轴属性
    CPTXYAxis *x                        = xyAxisSet.xAxis;
    CPTMutableTextStyle *whiteTextStyle = [[CPTMutableTextStyle alloc] init];
    whiteTextStyle.color    = [CPTColor whiteColor];
    whiteTextStyle.fontSize = CPTFloat(13.0);
    CPTMutableTextStyle *minorTickWhiteTextStyle = [[CPTMutableTextStyle alloc] init];
    minorTickWhiteTextStyle.color    = [CPTColor whiteColor];
    minorTickWhiteTextStyle.fontSize = CPTFloat(12.0);
    x.labelingPolicy                 = CPTAxisLabelingPolicyFixedInterval;
    x.majorIntervalLength            = CPTDecimalFromDouble(0.5);
    x.orthogonalCoordinateDecimal    = CPTDecimalFromDouble(0.0);
    x.tickDirection                  = CPTSignNone;
    x.minorTicksPerInterval          = 0;
    x.majorTickLineStyle             = majorLineStyle;
    x.minorTickLineStyle             = minorLineStyle;
    x.axisLineStyle                  = xyLineStyle;
    x.majorTickLength                = CPTFloat(7.0);
    x.minorTickLength                = CPTFloat(5.0);
    x.labelTextStyle                 = whiteTextStyle;
    x.minorTickLabelTextStyle        = minorTickWhiteTextStyle;
    x.titleTextStyle                 = whiteTextStyle;
    
    // 设置Y轴属性
    CPTXYAxis *y = xyAxisSet.yAxis;
    y.labelingPolicy              = CPTAxisLabelingPolicyFixedInterval;
    y.majorIntervalLength         = CPTDecimalFromDouble(0.5);
    y.minorTicksPerInterval       = 0;
    y.orthogonalCoordinateDecimal = CPTDecimalFromDouble(0.0);
    y.tickDirection               = CPTSignNone;
    y.majorTickLineStyle          = majorLineStyle;
    y.minorTickLineStyle          = minorLineStyle;
    y.axisLineStyle               = xyLineStyle;
    y.majorTickLength             = CPTFloat(7.0);
    y.minorTickLength             = CPTFloat(5.0);
    y.labelTextStyle              = whiteTextStyle;
    y.minorTickLabelTextStyle     = minorTickWhiteTextStyle;
    y.titleTextStyle              = whiteTextStyle;
    
    // 设置分割线
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    UIColor *majorGridLineColor = [UIColor colorWithHexString:COLOR_MAJOR_GRID_LINE];
    majorGridLineStyle.lineColor = [CPTColor colorWithCGColor:majorGridLineColor.CGColor];
    majorGridLineStyle.dashPattern = [NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:5], nil];
    majorGridLineStyle.lineWidth = CPTFloat(1.5);
    y.majorGridLineStyle          = majorGridLineStyle;
}


@end
