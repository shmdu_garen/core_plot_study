//
//  DSMStockPriceMovementUtil.h
//  core_plot_study
//
//  Created by dushumeng on 14-1-27.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#import "DSMStockBaseView.h"
#import "CorePlot-CocoaTouch.h"

@interface DSMStockPriceMovementUtil : NSObject
// 绘制股票走势X轴
+ (void)drawPriceMovementXAxis:(CPTXYGraph *)graph stockCode:(NSString *)code;

// 根据时间获得X轴的值
+ (NSNumber *)xValueFromData:(int)data stockCode:(NSString *)code;

+ (NSString *)dataFromXValue:(int)xValue stockCode:(NSString *)code;

+ (void)drawAxisLabel:(CPTXYAxis *)axis labels:(NSDictionary *)labels textColor:(NSString *)color;

+ (void)changeYAxisMajorTick:(CPTXYAxis *)yAxis start:(CGFloat)vStart end:(CGFloat)vEnd;

@end
