//
//  DSMStockHeader.h
//  core_plot_study
//
//  Created by dushumeng on 14-1-27.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#ifndef core_plot_study_DSMStockHeader_h
#define core_plot_study_DSMStockHeader_h

struct DSMStockPadding {
    CGFloat left;
    CGFloat top;
    CGFloat right;
    CGFloat bottom;
};
typedef struct DSMStockPadding DSMStockPadding;

struct DSMStockDrawInfo {
    BOOL showLeft;
    BOOL showRight;
    BOOL showTop;
    BOOL showBottom;
    BOOL showHorizontal;
};
typedef struct DSMStockDrawInfo DSMStockDrawInfo;

struct DSMStockGesture
{
    BOOL tap;
    BOOL pan;
    BOOL pinch;
};
typedef struct DSMStockGesture DSMStockGesture;

typedef enum
{
	EM_MARKET_ID_NORMAL_ALL = 0,
	EM_MARKET_ID_MIN =0,
    
	EM_MARKET_ID_NORMAL_MIN = 0,
	EM_MARKET_ID_SZ,
	EM_MARKET_ID_SH,
	EM_MARKET_ID_SB,
	EM_MARKET_ID_NORMAL_MAX,
	
	EM_MARKET_ID_FT_MIN = 10,
	EM_MARKET_ID_FT_ZCE,
	EM_MARKET_ID_FT_DCE,
	EM_MARKET_ID_FT_CFFEX,
	EM_MARKET_ID_FT_SH,
	EM_MARKET_ID_FT_MAX,
	
	EM_MARKET_ID_MAX,
	EM_MARKET_ID_FT_SHZQ = EM_MARKET_ID_MAX
}DSMMarketID;

#endif
