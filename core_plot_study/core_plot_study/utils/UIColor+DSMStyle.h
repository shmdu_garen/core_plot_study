//
//  UIColor+DSMStyle.h
//  core_plot_study
//
//  Created by dushumeng on 14-1-20.
//  Copyright (c) 2014年 garen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface UIColor (DSMStyle)

+ (UIColor *)colorWithHexString:(NSString *)colorStr;

@end


@interface CPTColor (DSMStyle)

+ (CPTColor *)colorWithHexString:(NSString *)colorStr;

@end
